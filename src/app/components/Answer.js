import React from 'react';
import "./Answer.css";

const Answer = (props) => {

    const { correct, wrong, answer, id, handleClickedAnswer, finish } = props;
    let rightAnswer = correct || answer;

    const displayAnswers = () => {
        if (correct) {
            return correct;
        }
        return wrong;
    };

    const colorAnswers = () => {
        let color;
        const { answers } = props;

        answers.map(givenAnswer => {
            const { answer, clickedResponse } = givenAnswer;
            const giveAnswerId = givenAnswer.id;
            if (giveAnswerId === id) {
                if (answer === false) {
                    return clickedResponse === wrong ? color = "wrong" : "";
                }
                if (answer === true) {
                    return clickedResponse === correct ? color = "right" : "";
                    }
            }
            return "";
        });

        return color;
    };

    return (
        <div className={!finish ? "div-hover" : ""}>
        <div className={finish ? colorAnswers() : ""} >
            <input type="radio" name={props.id} value={displayAnswers()} onClick={(e) => handleClickedAnswer(e, rightAnswer, id)} className="bla" />
            <span>{displayAnswers()}</span>
        </div>
        </div>
    );
};

export default Answer;